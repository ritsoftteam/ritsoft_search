-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2016 at 08:02 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ritsoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `batch_id` int(5) NOT NULL AUTO_INCREMENT,
  `year_batch` int(5) DEFAULT NULL,
  `course_spec_id` int(5) NOT NULL,
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`batch_id`, `year_batch`, `course_spec_id`) VALUES
(1, 2013, 1),
(2, 2014, 1),
(3, 2015, 1),
(4, 2015, 13),
(5, 2016, 13),
(6, 2010, 2),
(7, 2011, 2),
(8, 2015, 6),
(9, 2016, 4),
(10, 2015, 7),
(11, 2015, 7),
(12, 2016, 8),
(13, 2015, 6),
(14, 2015, 9),
(15, 2016, 9),
(16, 2015, 10),
(17, 2014, 12),
(18, 2015, 11),
(19, 2016, 12);

-- --------------------------------------------------------

--
-- Table structure for table `blood_group`
--

CREATE TABLE IF NOT EXISTS `blood_group` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `bgroup` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `blood_group`
--

INSERT INTO `blood_group` (`id`, `bgroup`) VALUES
(1, 'A+'),
(2, 'A-'),
(3, 'AB+'),
(4, 'AB-'),
(5, 'B+'),
(6, 'B-'),
(7, 'O+'),
(8, 'O-');

-- --------------------------------------------------------

--
-- Table structure for table `caste`
--

CREATE TABLE IF NOT EXISTS `caste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caste` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `caste`
--

INSERT INTO `caste` (`id`, `caste`) VALUES
(1, 'Ezhava'),
(2, 'Nair'),
(3, 'Kurava'),
(4, 'Pulaya'),
(5, 'Orthodox'),
(6, 'Catholica');

-- --------------------------------------------------------

--
-- Table structure for table `cc`
--

CREATE TABLE IF NOT EXISTS `cc` (
  `cc_no` int(5) NOT NULL AUTO_INCREMENT,
  `adm_no` varchar(10) NOT NULL,
  `chrctr` text NOT NULL,
  PRIMARY KEY (`cc_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cc`
--

INSERT INTO `cc` (`cc_no`, `adm_no`, `chrctr`) VALUES
(1, '16BC1', 'Good'),
(2, '16ME8', 'GOOD');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` varchar(5) NOT NULL,
  `course` varchar(20) NOT NULL,
  `category` varchar(20) NOT NULL,
  `no_of_semesters` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course`, `category`, `no_of_semesters`) VALUES
('1', 'Btech', 'ug', 8),
('2', 'Mtech', 'pg', 4),
('3', 'Barch', 'ug', 10),
('4', 'MCA', 'pg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `course_specialization`
--

CREATE TABLE IF NOT EXISTS `course_specialization` (
  `course_spec_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(5) NOT NULL,
  `spec_id` int(5) NOT NULL,
  PRIMARY KEY (`course_spec_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `course_specialization`
--

INSERT INTO `course_specialization` (`course_spec_id`, `course_id`, `spec_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 7),
(7, 2, 8),
(8, 2, 9),
(9, 2, 10),
(10, 2, 11),
(11, 2, 12),
(12, 3, 6),
(13, 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `role`) VALUES
('student', 'student', 'student'),
('staff', 'staff', 'staff'),
('admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `pg`
--

CREATE TABLE IF NOT EXISTS `pg` (
  `pg_id` int(5) NOT NULL,
  `admno` varchar(10) NOT NULL,
  `roll_no` text,
  `rank` text,
  `quota` text,
  `school_1` text,
  `reg_no_yr_1` text,
  `board_1` text,
  `school_2` text,
  `reg_no_yr_2` text,
  `board_2` text,
  `no_chance` text,
  `name_last` text,
  `total` text,
  `cur_sem` text,
  `gate_score` text NOT NULL,
  PRIMARY KEY (`pg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg`
--

INSERT INTO `pg` (`pg_id`, `admno`, `roll_no`, `rank`, `quota`, `school_1`, `reg_no_yr_1`, `board_1`, `school_2`, `reg_no_yr_2`, `board_2`, `no_chance`, `name_last`, `total`, `cur_sem`, `gate_score`) VALUES
(2, '16ME2', '6069692', '152', 'SM', 'KRKPM BHS, KADAMPANAD', '479038, 2008', 'BOARD OF PUBLIC EXAMINATION', 'CAS, ADOOR', '32010802009, 2013', 'KERALA UNIVERSITY', '1', 'CAS, ADOOR', '2.89', 'S1', '7.8'),
(3, '16MP3', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', 'S1', ''),
(4, '16MP4', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', 'S1', ''),
(6, '16MP6', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', 'S1', ''),
(7, '16MP7', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', 'S1', ''),
(8, '16ME8', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', 'S3', '22');

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE IF NOT EXISTS `religion` (
  `rl_id` int(5) NOT NULL AUTO_INCREMENT,
  `religion` text NOT NULL,
  PRIMARY KEY (`rl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`rl_id`, `religion`) VALUES
(1, 'Hindu'),
(2, 'Christian'),
(3, 'Islam'),
(4, 'Jainism'),
(5, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `religion_caste`
--

CREATE TABLE IF NOT EXISTS `religion_caste` (
  `rc_id` int(5) NOT NULL AUTO_INCREMENT,
  `religion` varchar(20) NOT NULL,
  `caste` varchar(20) NOT NULL,
  PRIMARY KEY (`rc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `religion_caste`
--

INSERT INTO `religion_caste` (`rc_id`, `religion`, `caste`) VALUES
(1, 'Hindu', ''),
(2, 'Christian', ''),
(3, 'Islam', ''),
(4, 'Others', '');

-- --------------------------------------------------------

--
-- Table structure for table `sem`
--

CREATE TABLE IF NOT EXISTS `sem` (
  `sem_id` int(2) NOT NULL,
  `semester` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sem`
--

INSERT INTO `sem` (`sem_id`, `semester`) VALUES
(1, 'S1'),
(2, 'S3');

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE IF NOT EXISTS `specialization` (
  `spec_id` varchar(2) NOT NULL,
  `specialisation` varchar(50) NOT NULL,
  PRIMARY KEY (`spec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialization`
--

INSERT INTO `specialization` (`spec_id`, `specialisation`) VALUES
('1', 'Civil Engineering'),
('10', 'Advanced Communication & Information System'),
('11', 'Advanced Electronics & Communication'),
('12', 'Computer Science & Engineering'),
('13', 'Computer Application'),
('2', 'Mechanical Engineering'),
('3', 'Electrical & Electronics Engineering'),
('4', 'Electronics & Communication Engineering'),
('5', 'Computer Science & Engineering'),
('6', 'Architecture'),
('7', 'Industrial Engineering & Management'),
('8', 'Industrial Drives & Control'),
('9', 'Transportation Engineering');

-- --------------------------------------------------------

--
-- Table structure for table `stud`
--

CREATE TABLE IF NOT EXISTS `stud` (
  `adm_no` varchar(10) NOT NULL,
  `yr_adm` text,
  `name` text,
  `dob` text,
  `sex` text,
  `religion` text,
  `caste` text,
  `mobile` text,
  `email` text,
  `name_guard` text,
  `relation` text,
  `occupation` text,
  `income` text,
  `address` text,
  `phone` text,
  `image` varchar(50) DEFAULT NULL,
  `admission_type` varchar(25) NOT NULL,
  `blood_group` varchar(5) NOT NULL,
  `stud_id` int(5) NOT NULL,
  PRIMARY KEY (`adm_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stud`
--

INSERT INTO `stud` (`adm_no`, `yr_adm`, `name`, `dob`, `sex`, `religion`, `caste`, `mobile`, `email`, `name_guard`, `relation`, `occupation`, `income`, `address`, `phone`, `image`, `admission_type`, `blood_group`, `stud_id`) VALUES
('16BA9', '2016-10-16', 'Harsha Sarath', '2016-07-05', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Nisha', 'Mother', 'Teacher', '100000', 'Athira, Edathitta', '04734222601', '9556aa.jpg', 'Normal', 'B-', 9),
('16BC1', '2016-09-26', 'Nithin Prasannan', '2016-09-01', 'M', 'Hindu', 'Ezhava', '9846261698', 'nithin2710@gmail.com', 'Prasannanandan', 'Father', 'Agriculture', '48000', 'Govindamandhiram, Nellimukal PO, Adoor', '04734222601', '31466myyyyyyyyy.JPG', 'Normal', 'AB+', 1),
('16BC5', '2016-10-16', 'Nithin P', '2016-10-04', 'M', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Prasanannandan', 'Father', 'Farmer', '100000', 'Govindamandhiram', '04734222601', '10060IMG_20160222_191751.jpg', 'Normal', 'AB+', 5),
('16ME2', '2016-09-26', 'Abhishek Pillai', '2016-01-02', 'F', 'Hindu', 'Nair', '9526961416', 'abhipins@gmail.com', 'Somanath', 'Father', 'Gulf', '75000', 'Abhinivas, Pnnivizha PO, Kodumon, Pathanamthitta', '04734222601', '23478IMG_20160222_191751.jpg', 'Normal', 'B+', 2),
('16ME8', '2016-10-16', 'Sree', '2016-06-08', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sree', 'Mother', 'Farmer', '100000', 'Sree Bhavan', '04734222601', '8550aa.jpg', 'Lateral', 'A-', 8),
('16MP3', '2016-10-16', 'Harsha Sarath', '2016-05-10', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sarath', 'Father', 'Farmer', '100000', 'adoor', '04734222601', '4908aa.jpg', 'Lateral', 'A-', 3),
('16MP4', '2016-10-16', 'Harsha Sarath', '2016-05-10', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sarath', 'Father', 'Farmer', '100000', 'adoor', '04734222601', 'student_2-wallpaper-3840x2160.jpg', 'Lateral', 'A-', 4),
('16MP6', '2016-10-16', 'Harsha Sarath', '2016-05-10', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sarath', 'Father', 'Farmer', '100000', 'adoor', '04734222601', '4908aa.jpg', 'Lateral', 'A-', 6),
('16MP7', '2016-10-16', 'Nithin P', '2016-06-08', 'M', 'Hindu', 'Ezhava', '9846261698', 'nithin2710@gmail.com', 'Prasanannandan', 'Father', 'Farmer', '100000', 'Govindamandhiram', '04734222601', '18088myyyyyyyyy.JPG', 'Normal', 'AB+', 7);

-- --------------------------------------------------------

--
-- Table structure for table `stud_batch_rel`
--

CREATE TABLE IF NOT EXISTS `stud_batch_rel` (
  `adm_no` varchar(10) NOT NULL,
  `batch_id` varchar(2) NOT NULL,
  `stud_batch_rel_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stud_batch_rel`
--

INSERT INTO `stud_batch_rel` (`adm_no`, `batch_id`, `stud_batch_rel_id`) VALUES
('16BC1', '1', 1),
('16ME2', '11', 2),
('16MP3', '4', 3),
('16MP4', '4', 4),
('16BC5', '3', 5),
('16MP6', '4', 6),
('16MP7', '4', 7),
('16ME8', '11', 8),
('16BA9', '17', 9);

-- --------------------------------------------------------

--
-- Table structure for table `tc`
--

CREATE TABLE IF NOT EXISTS `tc` (
  `tc_no` int(5) NOT NULL AUTO_INCREMENT,
  `adm_no` varchar(10) NOT NULL,
  `tc_date` text NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`tc_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tc`
--

INSERT INTO `tc` (`tc_no`, `adm_no`, `tc_date`, `reason`) VALUES
(1, '16BC1', '2016-10-15', 'Transfer'),
(2, '16ME8', '2016-10-17', 'AAA');

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE IF NOT EXISTS `temp` (
  `temp_no` int(10) NOT NULL AUTO_INCREMENT,
  `course` varchar(10) NOT NULL,
  `specialisation` varchar(50) NOT NULL,
  `batch` int(5) NOT NULL,
  `yr_adm` text,
  `name` text,
  `dob` text,
  `sex` text,
  `religion` text,
  `caste` text,
  `mobile` text,
  `email` text,
  `name_guard` text,
  `relation` text,
  `occupation` text,
  `income` text,
  `address` text,
  `phone` text,
  `image` varchar(100) DEFAULT NULL,
  `roll_no` text,
  `rank` text,
  `quota` text,
  `school_1` text,
  `reg_no_yr_1` text,
  `board_1` text,
  `school_2` text,
  `reg_no_yr_2` text,
  `board_2` text,
  `no_chance` text,
  `name_last` text,
  `total` text,
  `physics` text,
  `chemistry` text,
  `maths` text,
  `cur_sem` text,
  `admission_type` varchar(25) NOT NULL,
  `blood_group` varchar(5) NOT NULL,
  `gate_score` text NOT NULL,
  PRIMARY KEY (`temp_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`temp_no`, `course`, `specialisation`, `batch`, `yr_adm`, `name`, `dob`, `sex`, `religion`, `caste`, `mobile`, `email`, `name_guard`, `relation`, `occupation`, `income`, `address`, `phone`, `image`, `roll_no`, `rank`, `quota`, `school_1`, `reg_no_yr_1`, `board_1`, `school_2`, `reg_no_yr_2`, `board_2`, `no_chance`, `name_last`, `total`, `physics`, `chemistry`, `maths`, `cur_sem`, `admission_type`, `blood_group`, `gate_score`) VALUES
(1, 'Btech', 'Civil Engineering', 2013, '2016-09-26', 'Nithin Prasannan', '2016-09-01', 'M', 'Hindu', 'Ezhava', '9846261698', 'nithin2710@gmail.com', 'Prasannanandan', 'Father', 'Agriculture', '48000', 'Govindamandhiram, Nellimukal PO, Adoor', '04734222601', '31466myyyyyyyyy.JPG', '6069692', '152', 'SM', 'KRKPM BHS, KADAMPANAD', '479038, 2008', 'BOARD OF PUBLIC EXAMINATION', 'CAS, ADOOR', '32010802009, 2013', 'KERALA UNIVERSITY', '1', 'CAS, ADOOR', '2.89', '164', '152', '157', 'S1', 'Normal', 'AB+', ''),
(2, 'Btech', 'Civil Engineering', 2014, '2016-09-26', 'Harsha', '2016-09-01', 'F', 'Hindu', 'Ezhava', '9846261698', 'harsha2710@gmail.com', 'Nirmala', 'Mother', 'Housewife', '48000', 'Harsha bhavan, Kollam', '04682285629', '17528aa.jpg', '6069692', '152', 'SM', 'KRKPM BHS, KADAMPANAD', '479038, 2008', 'BOARD OF PUBLIC EXAMINATION', 'CAS, ADOOR', '32010802009, 2013', 'KERALA UNIVERSITY', '1', 'CAS, ADOOR', '2.89', '164', '152', '157', 'S3', 'Normal', 'A+', ''),
(3, 'Mtech', 'Industrial Drives & Control', 2015, '2016-09-26', 'Abhishek Pillai', '2016-01-02', '', 'Hindu', 'Nair', '9526961416', 'abhipins@gmail.com', 'Somanath', 'Father', 'Gulf', '75000', 'Abhinivas, Pnnivizha PO, Kodumon, Pathanamthitta', '04734222601', '23478IMG_20160222_191751.jpg', '6069692', '152', 'SM', 'KRKPM BHS, KADAMPANAD', '479038, 2008', 'BOARD OF PUBLIC EXAMINATION', 'CAS, ADOOR', '32010802009, 2013', 'KERALA UNIVERSITY', '1', 'CAS, ADOOR', '2.89', NULL, NULL, NULL, 'S1', 'Normal', 'B+', '7.8'),
(7, 'Btech', 'Civil Engineering', 2015, '2016-10-16', 'Nithin P', '2016-10-04', 'M', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Prasanannandan', 'Father', 'Farmer', '100000', 'Govindamandhiram', '04734222601', '10060IMG_20160222_191751.jpg', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', '145', '165', '156', 'S1', 'Normal', 'AB+', ''),
(8, 'MCA', 'Computer Application', 2015, '2016-10-16', 'Harsha Sarath', '2016-05-10', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sarath', 'Father', 'Farmer', '100000', 'adoor', '04734222601', '4908aa.jpg', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', NULL, NULL, NULL, 'S1', 'Lateral', 'A-', ''),
(9, 'MCA', 'Computer Application', 2015, '2016-10-16', 'Nithin P', '2016-06-08', 'M', 'Hindu', 'Ezhava', '9846261698', 'nithin2710@gmail.com', 'Prasanannandan', 'Father', 'Farmer', '100000', 'Govindamandhiram', '04734222601', '18088myyyyyyyyy.JPG', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', NULL, NULL, NULL, 'S1', 'Normal', 'AB+', ''),
(10, 'Mtech', 'Industrial Drives & Control', 2015, '2016-10-16', 'Sree', '2016-06-08', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Sree', 'Mother', 'Farmer', '100000', 'Sree Bhavan', '04734222601', '8550aa.jpg', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', NULL, NULL, NULL, 'S3', 'Lateral', 'A-', '22'),
(11, 'Barch', 'Architecture', 2014, '2016-10-16', 'Harsha Sarath', '2016-07-05', 'F', 'Hindu', 'Ezhava', '9846261698', 'abhi@gmail.com', 'Nisha', 'Mother', 'Teacher', '100000', 'Athira, Edathitta', '04734222601', '9556aa.jpg', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', '145', '165', '156', 'S1', 'Normal', 'B-', '');

-- --------------------------------------------------------

--
-- Table structure for table `ug`
--

CREATE TABLE IF NOT EXISTS `ug` (
  `ug_id` int(5) NOT NULL,
  `admno` varchar(10) NOT NULL,
  `roll_no` text,
  `rank` text,
  `quota` text,
  `school_1` text,
  `reg_no_yr_1` text,
  `board_1` text,
  `school_2` text,
  `reg_no_yr_2` text,
  `board_2` text,
  `no_chance` text,
  `name_last` text,
  `total` text,
  `physics` text,
  `chemistry` text,
  `maths` text,
  `cur_sem` text,
  PRIMARY KEY (`ug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ug`
--

INSERT INTO `ug` (`ug_id`, `admno`, `roll_no`, `rank`, `quota`, `school_1`, `reg_no_yr_1`, `board_1`, `school_2`, `reg_no_yr_2`, `board_2`, `no_chance`, `name_last`, `total`, `physics`, `chemistry`, `maths`, `cur_sem`) VALUES
(1, '16BC1', '6069692', '152', 'SM', 'KRKPM BHS, KADAMPANAD', '479038, 2008', 'BOARD OF PUBLIC EXAMINATION', 'CAS, ADOOR', '32010802009, 2013', 'KERALA UNIVERSITY', '1', 'CAS, ADOOR', '2.89', '164', '152', '157', 'S1'),
(5, '16BC5', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', '145', '165', '156', 'S1'),
(9, '16BA9', '200', '13', 'SM', 'KRKPM ADOOR', '2008', 'KERALA', 'CAS ADOOR', '2013', 'KERALA', '1', 'CAS ADOOR', '2.87', '145', '165', '156', 'S1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
