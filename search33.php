<div class="boxHead" style="padding-top:1.5%; height:auto;">Student List</div>

<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet">
<!--<link href="css/style1.css" rel="stylesheet"> -->

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>
  <body>
  
   <?php
	  include("dboperation.php");
		//  $obj = new dboperation();
		//  $objz = new dboperation();
		  ?>
<br>
<br>
<table id="myTable" bordercolorlight="#C136F5" bgcolor="#B8D8E4">
<thead>
								<th>Adm No</th>
						  		<th>Name</th>
								   <th>Course</th>
                                  <th>Specialization</th>
						
									<th>Blood Group</th>
                                    <th>Mobile</th>
								  <th>Email</th>
									</thead>
                                   <?php

	$obj5=new dboperation();
	$obj6=new dboperation();
	$obj3=new dboperation();
	$obj2=new dboperation();
	$obj4=new dboperation();
	$obj6=new dboperation();
	$obj8=new dboperation();
	
	$query5="select id,course from courses where category='pg'";
	$result5=$obj5->selectdata($query5);
	while($row=$obj5->fetch($result5))
	{
		$id=("$row[0]");
		$course=("$row[1]");
		$query6="select spec_id from course_specialization where course_id=$id";
		$result6=$obj6->selectdata($query6);
		while($row=$obj6->fetch($result6))
		{
	 		$spid=("$row[2]");
			$query3="select specialisation from specialization  where spec_id=$spid";
			$result3=$obj3->selectdata($query3);
			while($row=$obj3->fetch($result3))
			{
				$spzn=$row[1];
				$query2="select course_spec_id from course_specialization where course_id=$id and spec_id=$spid";
				$result2=$obj2->selectdata($query2);
				while($row=$obj2->fetch($result2))
				{
					$b=("$row[0]");
					$query4="select batch_id,year_batch from batch where course_spec_id=$b";
					$result4=$obj4->selectdata($query4);
					while($row=$obj4->fetch($result4))
						{
							$bid=("$row[0]");
							$yb=("$row[1]");
							$query6="select adm_no from stud_batch_rel where batch_id=$bid";
							$result6=$obj6->selectdata($query6);
							while($row=$obj6->fetch($result6))
							{
								$specialisation=$row[1]; 
	}
	
	
	
	
	
	
	
	
	
	
	
	$query5="SELECT * FROM stud";	
	$result5=$obj5->selectdata($query5);
	while($row=$obj5->fetch($result5))
	{
	 /*	$adno=("$row[0]");
		$query6="SELECT * FROM stud_batch_rel WHERE adm_no = '$ad_no' ";	
		$result6=$obj6->selectdata($query6);
		while($row=$obj6->fetch($result6))
		{
	 		$bchid=("$row[1]");
	
		$query3="SELECT * FROM batch WHERE batch_id = '$bchid' ";
		$result3=$obj3->selectdata($query3);
		while($row=$obj3->fetch($result3))
		{
			$cs_id=$row[2];
	
			$query2="SELECT * FROM course_specialization WHERE course_spec_id = '$cs_id' ";	
			$result2=$obj2->selectdata($query2);
			while($row=$obj2->fetch($result2))
			{
	 			$cid=("$row[1]");
	    		$sid=("$row[2]");
	
			$query4="SELECT * FROM courses WHERE id = '$cid' ";
				$result4=$obj4->selectdata($query4);
				while($row=$obj4->fetch($result4))
				{
					$course=$row[1];
					$category=$row[2]; 
	
					$query6="SELECT * FROM specialization WHERE spec_id = '$sid' ";
					$result6=$obj6->selectdata($query6);
					while($row=$obj6->fetch($result6))
					{
						$specialisation=$row[1]; 
						
						$query8="SELECT stud.adm_no,stud.name,stud.mobile,stud.email,stud.blood_group,courses.course,specialization.specialisation,batch.year_batch FROM stud,courses,specialization,batch WHERE stud.adm_no = '$adno' AND courses.id='$cid' AND courses.category='PG' AND specialization.spec_id='$sid' AND batch.batch_id='$bchid'";
						$result8=$obj8->selectdata($query8);
						while($row=$obj8->fetch($result8)) */
						//{
	 						$adno=("$row[0]");
							$name=("$row[1]");
							$mobile=("$row[2]");
							$email=("$row[3]");
							$blood=("$row[4]");
							$course=("$row[5]");
							$specialisation=("$row[6]");
							$batch=("$row[7]");
								?>
                                    <tr>
									<td><?php echo $adno;?></td>
									<td><?php echo $name;?></td>
									<td><?php echo $course;?></td>
									<td><?php echo $specialisation;?></td>
									<td><?php echo $batch;?></td>
                                    <td><?php echo $blood;?></td>
                                    <td><?php echo $mobile;?></td>
									
									</tr>
									<?php
								}
			//		}
			//	}
		//	}
	//	}
	//	}
	//} 
								?>
                                     </table>
</body>
</html>
